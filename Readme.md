# NSQ is a realtime distributed messaging platform.

To start run the following command from the same directory as the docker-compose.yml created previously.

```docker-compose up -d```

A private network will be created and three containers will be started using the private network. On the local host each container will have a random port mapped to ports exposed in the docker-compose.yml.

To view the running containers status and mapped ports.

```docker-compose ps```

To see the logs from the running containers.

```docker-compose logs```

Assuming that the nsqlookupd had host port 31001 mapped to the container port 4161 a simple ping could be performed using curl.

```curl http://127.0.0.1:31001/ping```

[Reference](https://nsq.io/deployment/docker.html)

# Usage
## go-nsq

[![Build Status](https://github.com/nsqio/go-nsq/workflows/tests/badge.svg)](https://github.com/nsqio/go-nsq/actions) [![GoDoc](https://godoc.org/github.com/nsqio/go-nsq?status.svg)](https://godoc.org/github.com/nsqio/go-nsq) [![GitHub release](https://img.shields.io/github/release/nsqio/go-nsq.svg)](https://github.com/nsqio/go-nsq/releases/latest)

The official Go package for [NSQ][nsq].

### Docs

See [godoc][nsq_gopkgdoc] and the [main repo apps][apps] directory for examples of clients built
using this package.

### Tests

Tests are run via `./test.sh` (which requires `nsqd` and `nsqlookupd` to be installed).

[nsq]: https://github.com/nsqio/nsq
[nsq_gopkgdoc]: http://godoc.org/github.com/nsqio/go-nsq
[apps]: https://github.com/nsqio/nsq/tree/master/apps
[travis]: http://travis-ci.org/nsqio/go-nsq